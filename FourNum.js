const FourNumbers = (array, targetSum) =>{

    // hash map to store values
let arrLoop ={}
let fourNum =[]

for(let i=1; i< array.length-1; i++){
  for(let j=i+1; j<array.length; j++){
// ^^prevents index from being used twice
    let sum = array[i] + array[j]
    let sub = targetSum - sum
    // ^^sum & sub used to find corresponding values
    if(sub in arrLoop){
      arrLoop.forEach(item=>{
        fourNum.push([...item, array[i], array[j]])
      })
    }
  }
  for(let k = 0; k < i; k++){
    // ^^ loop to mark current values as true
    let sum= array[i] + array[k]
    if(sum in arrLoop){
      arrLoop[sum].push([array[i], array[k]])
    }
    else{
      arrLoop[sum] = [[array[i], array[k]]]
    }
  }
}
// groups & sorts two-dimensional array of quadruplets
let sortArr= fourNum.map(item=>item.sort((a,b)=>a-b))
let set = new Set(sortArr.map(item=>item.join(',')))
let newArr = []
set.forEach(item=>newArr.push([...item.split(',')]))

return newArr

}
FourNumbers([7, 6, 4, -1, 1, 2], 16)