# Theta-Coding-Challenge

The problems I chose to complete are: Water Area & Four Numbers. 


### Execute Functions
1. Download files 
2. Open files in VS Code
3. Execute function, run following command in terminal:

    -- node FourNums.js

    -- node WaterPillar.js

    -- node RiverSizes.js

    -- node Palidrome.js

(Or copy and paste code in a web browser console) 


### Water Area

Requirement:

Write a function that calculates the area of water between pillars. 

Solution
1. Find the height of pillars going from left to right and then right to left.
2. Compare the difference in height between the pillars 
3. Determine the area of water between the pillars based off of the difference in height. 



### Four FourNumbers

Requirement: 

Write a function that takes in a non-empty array of distinct integers and an integer representing a target sum.

Solution
1. Hash map to store array values
2. 1st Loop that prevents duplicate Numbers
3. 2nd Loop to prevent index from being used twice
4. 3rd Loop Marks current values as true




### River Sizes Solution 

Requirement:

Write a function that returns an array of the sizes of all rivers represented in the input matrix. The sizes don't need to be in any particular order.

Solution:
1. Outer Function (RiverSizes) used to find starting point of river
2. Inner Function (ComparePoints) used to determine length of river & returns number 
