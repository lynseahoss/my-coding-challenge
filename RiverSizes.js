const matrix = [
  [1, 0, 0, 1, 0],
  [1, 0, 1, 0, 0],
  [0, 0, 1, 0, 1],
  [1, 0, 1, 0, 1],
  [1, 0, 1, 1, 0],
];

const comparePoints = (x, y, matrix) => {
    const queue = [{ x, y }];
    let size = 0;
    while (queue.length) {
      const { x, y } = queue.shift();
      size += 1;
      matrix[y][x] = 0;
      [
        [x + 1, y],
        [x, y + 1],
        [x - 1, y],
        [x, y - 1],
      ].forEach(([i, j]) => {
        if (matrix[j] && matrix[j][i]) {
          queue.push({ x: i, y: j, size: size + 1 });
        }
      });
    }
    return size;
  };

  
const RiverSizes = (matrix) => {
    let results = [];
    matrix.forEach((horz, y) => {
        horz.forEach((vert, x) => {
            if (matrix[y][x] === 1) {
                results.push(comparePoints(x, y, matrix));
            }
        });
    });
    return results;
};
RiverSizes(matrix);

