const WaterTrapped = (heights) => {
    // heights is used as an array for the pillars to trap the water
    
    if (heights === 0) {
      return 0;
    }
    let waterArea = 0;
    let length = heights.length;
    let leftPillar = [];
    let rightPillar = [];
  
    leftPillar[0] = heights[0];
  
    // finds potential heights of leftPillar
    for (let i = 1; i < length; i++) {
      leftPillar[i] = Math.max(heights[i], leftPillar[i - 1]);
    }
  
    rightPillar[length - 1] = heights[length - 1];
  
    //finds potential heights of rightPillar
    for (let i = length - 2; i >= 0; i--) {
      rightPillar[i] = Math.max(heights[i], rightPillar[i + 1]);
    }
  
    // find heights of waterTrapped using min values of leftPillar & rightPillar by subtracting heights array
    for (let i = 1; i < length - 1; i++) {
      // water wouldn't be trapped between pillars if i=0
      waterArea += Math.min(leftPillar[i], rightPillar[i]) - heights[i];
    }
    return waterArea
  };
  WaterTrapped([0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3])